module tuorGo

go 1.16

require (
	github.com/codegangsta/negroni v1.0.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/gorilla/sessions v1.2.1 // indirect
	github.com/markbates/goth v1.67.1 // indirect
)
