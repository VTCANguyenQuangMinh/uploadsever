package main

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"
	"github.com/markbates/goth"
	"github.com/markbates/goth/gothic"
	"github.com/markbates/goth/providers/google"
)

func init() {
	key := "Secret-session-key"
	maxAge := 86400 * 30
	isProd := false

	store := sessions.NewCookieStore([]byte(key))
	store.MaxAge(maxAge)
	store.Options.Path = "/"
	store.Options.HttpOnly = true
	store.Options.Secure = isProd

	gothic.Store = store

	goth.UseProviders(
		google.New("526415377132-l5vqjhut61mvv0f9oht193s94lk2knlf.apps.googleusercontent.com", "-qCtKd77ZanlDspK16poa8kW", "http://localhost:8080/auth/google/callback", "email", "profile"),
	)
}

func UploadFile(w http.ResponseWriter, r *http.Request) {
	maxMemmory := 5 * 1024 * 1024
	r.ParseMultipartForm(int64(maxMemmory))

	file, handler, err := r.FormFile("myFile")
	if err != nil {
		fmt.Println("Error retriving the file")
		fmt.Println(err)
		return
	}
	defer file.Close()

	if handler.Size > 2<<20 {
		fmt.Println("Error: The file size is too large")
		t, _ := template.ParseFiles("template/false.html")
		t.Execute(w, nil)
		return
	}

	if err != nil {
		fmt.Println(err)
	}

	f, err := os.OpenFile("files/"+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println("save file error!")
		fmt.Println(err)
		return
	}

	defer f.Close()
	io.Copy(f, file)
	fmt.Println("upload file successful")
	t, _ := template.ParseFiles("template/success.html")
	t.Execute(w, nil)
}

func ViewIndex(w http.ResponseWriter, r *http.Request) {

	_, err := gothic.CompleteUserAuth(w, r)
	if err != nil {
		fmt.Fprintln(w, err)
		return
	}

	t, err := template.ParseFiles("template/index.html")
	if err != nil {
		fmt.Println("Using template error")
		return
	}
	err = t.Execute(w, nil)
	if err != nil {
		fmt.Println(err)
		return
	}
}

func Login(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("template/login.html")
	t.Execute(w, false)
}

func Authentication(w http.ResponseWriter, r *http.Request) {
	gothic.BeginAuthHandler(w, r)
}

func SetupRouters(router *mux.Router) *mux.Router {
	Router := mux.NewRouter()
	Router.HandleFunc("/", Login).Methods("GET")
	Router.HandleFunc("/auth/{provider}/callback", ViewIndex).Methods("GET")
	Router.HandleFunc("/auth/{provider}", Authentication).Methods("GET")
	Router.HandleFunc("/upload", UploadFile).Methods("POST")

	return Router
}

func main() {
	router := mux.NewRouter()
	router = SetupRouters(router)
	log.Println("Listening...")
	log.Fatal(http.ListenAndServe(":8080", router))
}
